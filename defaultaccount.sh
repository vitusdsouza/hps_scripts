#!/bin/bash

############### defaultaccount ###############
function defaultaccount () {
	/scripts/generate_maildirsize --confirm $username
	echo "Done"
}

##############################################

############### Run Script ###############
[ $# -eq 0 ] && { echo "Usage: $0 <domain>"; exit 1; }
username=$(grep $1 /etc/trueuserdomains | tail -n 1 | cut -d ":" -f1)
if [ -z "$username" ]; then
	echo "Usage: $0 <domain>"; 
	exit 1
fi

defaultaccount
############### End Script ###############