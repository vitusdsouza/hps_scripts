#!/bin/bash


function get_domain () {
	truedomain=$(grep $username /etc/trueuserdomains | cut -d":" -f1)
	if [[ -s "$truedomain" ]]; then
		echo -e "Enter a valid CPanel Username"
		exit 1
	fi
}

function get_domlog_files () {
	domlog_files=$(ls /usr/local/apache/domlogs/ | grep $truedomain | grep -v ftp | grep -v bytes)
	if [[ $? -ne 0 ]]; then
		echo -e "Domain access log file missing on server. Kindly verify the CPanel Username."
		exit 1
	fi
}


function get_initial_timestamp () {
	start_timestamp=$(head -n 1 /usr/local/apache/domlogs/$n | awk '{print $4,$5}')
}

function get_post_hit () {
	post_hit=$(cat /usr/local/apache/domlogs/$n | grep POST | grep " 200 " | cut -d" " -f7 | sort | uniq | cut -d"/" -f2-100 | grep -v "wp-cron.php?doing_wp_cron")
}

function get_country () {
	stat /usr/bin/whois>/dev/null 2>&1
	if [[ $? -ne 0 ]]; then
		country=" "
	else
		country=$(whois $ip | grep country | tail -n 1 | awk '{print $2}')
	fi
}

function get_hit_timestamp () {
	for t in $post_hit
	do
		iptime=$(grep $t /usr/local/apache/domlogs/$n | grep POST | grep " 200 " | tail -n 1 | awk '{print $1,$4,$5}')
		ip=$(echo $iptime | cut -d" " -f1)
		timestamp=$(echo $iptime | cut -d" " -f2)
		hit_count=$(grep $t /usr/local/apache/domlogs/$n | grep POST | grep " 200 " | wc -l)
		get_country
		echo "$ip" "$country" "$timestamp]" "[Hits:$hit_count]" $t
	done
}

read -p "Enter CPanel Username: " username

check_cmd
get_domain
get_domlog_files

for n in $domlog_files
do
	data=$(cat /usr/local/apache/domlogs/$n | grep POST | grep " 200 " | grep -v "wp-cron.php?doing_wp_cron")
	if [[ -z $data ]]; then
		:
	else
		echo " "
		echo "Parsing the log file: " $n
		get_initial_timestamp
		echo "Logs parsed since:" $start_timestamp
		echo " "
		get_post_hit
		get_hit_timestamp
		echo " "
	fi
done