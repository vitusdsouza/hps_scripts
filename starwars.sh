#!/bin/bash
#Author: Vitus D'Souza

###############Common###############

function get_cpanel_username {
	read -p "Enter CPanel Username or Domain Name: " namespace
	username=$(grep $namespace /etc/trueuserdomains | cut -d " " -f2)
	truedomain=$(grep $namespace /etc/trueuserdomains | cut -d":" -f1)
	path=$(grep $username /etc/userdatadomains | cut -d"=" -f9 | cut -d"/" -f1,2,3)
	if [[ -s "$truedomain" ]]; then
		echo -e "Enter a valid CPanel Username"
		return
	fi
}

function check_if_integer () {
	if ! [[ "$integer" =~ ^[0-9]+$ ]]
	    then
	        echo "Sorry integers only"
			return
	fi
}

function get_date {
	read -p "Enter date in MM-DD-YYYY Format: " date
	d=${date:3:2}; m=${date:0:2}; Y=${date:6:4}; 
	echo "Year=$Y, Month=$m, Day=$d"
	if date -d "$Y-$m-$d" &> /dev/null; then 
		echo "Date is Valid. Proceeding..."
		new_date=$(echo $Y-$m-$d)
	else 
		echo "Invalid Date. Aborting..."
		return
	fi
}



###############ClamAV Scan###############


function scan () {
	get_cpanel_username
	screen -dmS $truedomain.scan bash -c 'scan -p $path; exec sh'
	echo " "
	echo "Scan for user directory $path has been started."
	echo " "
	echo "Press ENTER To Continue..."
	read
	return
}



###############CPLogTable###############



function cplog () {
	get_cpanel_username
	python <(curl -ks https://gitlab.com/deepslate/MyBash/-/raw/master/gcar_generate_cpanel_access_log_report.py) $truedomain
	echo "Press ENTER To Continue..."
	read
	return
}



###############Domain Access Log Parser###############



function domlog () {
	get_cpanel_username
	get_domlog_files
	for n in $domlog_files
	do 
	data=$(cat /usr/local/apache/domlogs/$n | grep POST | grep " 200 " | grep -v "wp-cron.php?doing_wp_cron" | egrep -v "115.110.127.198|122.15.255.69|115.110.71.146|182.73.214.22|111.93.159.50|220.227.162.29|115.110.126.75|182.74.165.114")
	if [[ -z $data ]]; then
		:
	else
		echo " "
		echo "Parsing the log file: " $n
		get_initial_timestamp
		echo "Logs parsed since:" $start_timestamp
		echo " "
		get_post_hit
		get_hit_timestamp
	fi
	done
	echo "Press ENTER To Continue..."
	read
	return
}

function get_domlog_files () {
	domlog_files=$(ls /usr/local/apache/domlogs/ | grep $truedomain | grep -v ftp | grep -v bytes)
	if [[ $? -ne 0 ]]; then
		echo -e "Domain access log file missing on server. Kindly verify the CPanel Username."
		return
	fi
}

function get_initial_timestamp () {
	start_timestamp=$(head -n 1 /usr/local/apache/domlogs/$n | awk '{print $4,$5}')
}

function get_post_hit () {
	post_hit=$(cat /usr/local/apache/domlogs/$n | grep POST | grep " 200 " | cut -d" " -f7 | sort | uniq | cut -d"/" -f2-100 | grep -v "wp-cron.php?doing_wp_cron")
}

function get_country () {
	stat /usr/bin/whois>/dev/null 2>&1
	if [[ $? -ne 0 ]]; then
		country=" "
	else
		country=$(whois $ip | grep country | tail -n 1 | awk '{print $2}')
	fi
}

function get_hit_timestamp () {
	for t in $post_hit
	do
		iptime=$(grep $t /usr/local/apache/domlogs/$n | grep POST | grep " 200 " | tail -n 1 | awk '{print $1,$4,$5}')
		ip=$(echo $iptime | cut -d" " -f1)
		timestamp=$(echo $iptime | cut -d" " -f2)
		hit_count=$(grep $t /usr/local/apache/domlogs/$n | grep POST | grep " 200 " | wc -l)
		get_country
		echo "$ip" "$country" "$timestamp]" "[Hits:$hit_count]" $t
	done
}



###############Ctime###############



function ctimelog() {
	get_cpanel_username
	get_ctime_choice
	echo "Press ENTER To Continue..."
	read
	return
}

function get_ctime_choice {
	read -p "Did the client mention the date on which client was hacked?  [Y/n]  " choice
	case $choice in
	[yY] )
		get_date
		get_ctime1
		;;
	[nN] )
		get_ctime2
		;;
	*)
		echo "Invalid Response. Aborting..."
		return
		;;
	esac
}

function get_ctime1 {
	read -p "Enter the Maximum number of files to be displayed (example: 50): " integer
	check_if_integer
	echo "Here are the files indicating a ctime modification on $new_date: "
	echo " "
	find $path -type f -print0 | xargs -0 stat --format '%Z :%z %n' | egrep -v "access-logs|bin|.cache|.config|.cpanel|.cphorde|etc|.htpasswds|.local|logs|mail|perl5|php|.pki|public_ftp|.softaculous|.spamassassin|.ssh|ssl|.subaccounts|tmp|.trash" | sort -nr | cut -d: -f2- | grep $new_date | head -n $integer
}

function get_ctime2 {
	read -p "Enter the Maximum number of files to be displayed (example: 50): " integer
	check_if_integer
	echo "Here are files with the latest ctime modification: "
	echo " "
	find $path -type f -print0 | xargs -0 stat --format '%Z :%z %n' | egrep -v "access-logs|bin|.cache|.config|.cpanel|.cphorde|etc|.htpasswds|.local|logs|mail|perl5|php|.pki|public_ftp|.softaculous|.spamassassin|.ssh|ssl|.subaccounts|tmp|.trash" | sort -nr | cut -d: -f2- | head -n $integer
}



###############Mtime###############



function mtimelog() {
	get_cpanel_username
	get_mtime_choice
	echo "Press ENTER To Continue..."
	read
	return
}

function get_mtime_choice {
	read -p "Did the client mention the date on which client was hacked?  [Y/n]  " choice
	case $choice in
	[yY] )
		get_date
		get_mtime1
		;;
	[nN] )
		get_mtime2
		;;
	*)
		echo "Invalid Response. Aborting..."
		return
		;;
	esac
}

function get_mtime1 {
	read -p "Enter the Maximum number of files to be displayed (example: 50): " integer
	check_if_integer
	echo "Here are the files indicating a mtime modification on $new_date: "
	echo " "
	find $path -type f -print0 | xargs -0 stat --format '%Y :%y %n' | egrep -v "access-logs|bin|.cache|.config|.cpanel|.cphorde|etc|.htpasswds|.local|logs|mail|perl5|php|.pki|public_ftp|.softaculous|.spamassassin|.ssh|ssl|.subaccounts|tmp|.trash" | sort -nr | cut -d: -f2- | grep $new_date | head -n $integer
}

function get_mtime2 {
	read -p "Enter the Maximum number of files to be displayed (example: 50): " integer
	check_if_integer
	echo "Here are files with the latest mtime modification: "
	echo " "
	find $path -type f -print0 | xargs -0 stat --format '%Y :%y %n' | egrep -v "access-logs|bin|.cache|.config|.cpanel|.cphorde|etc|.htpasswds|.local|logs|mail|perl5|php|.pki|public_ftp|.softaculous|.spamassassin|.ssh|ssl|.subaccounts|tmp|.trash" | sort -nr | cut -d: -f2- | head -n $integer
}



###############Quarantine###############



function quarantine () {
	read -p "Did you confirm the Reseller Support PIN? [Y/n]: " pin
	case $pin in
	[yY] )
		get_cpanel_username
		move_malware
		;;
	[nN] )
		echo "Nope, Not Happening. You need to confirm the 4-digit support PIN."
		return
		;;
	*)
		echo "Invalid Response. Aborting..."
		return
		;;
	esac
	echo "Press ENTER To Continue..."
	read
	return
}

function move_malware () {
	malware=$(find $path -maxdepth 1 -type f -name malware.txt)
	stat $path/malware.txt>/dev/null 2>&1
	if [[ $? -ne 0 ]]; then
		echo "File $path/malware.txt Not Found. Kindly run a ClamAV Scan First."
		return
	fi
	mkdir $path/quarantine
	chown $user:$user $path/quarantine
	if [[ $? -ne 0 ]]; then
		echo "Unable to create the quarantine directory"
		return
	else
		echo "The quarantine directory is: $path/quarantine"
	fi
	echo "Starting the Quarantine Process..."
	cat $path/malware.txt | awk '{print $1}' | sed 's/.$//' | grep $path > $path/quarantine/list
	num=0
	for q in `cat $path/quarantine/list`
	do
		quarantine_dir=$(dirname `echo $q` | cut -d"/" -f4-)
		mkdir -p $path/quarantine/$quarantine_dir
		mv $q $path/quarantine/$quarantine_dir/
		((num=num+1))
	done
	find $path/quarantine/ -type d -exec chmod 750 {} \;
	find $path/quarantine/ -type f -exec chmod 640 {} \;
	echo "$num Infected Files have been moved to $path/quarantine"
	chown -R $user:$user $path/quarantine
}



###############WPChecksum###############



function wpchecksum () {
	get_cpanel_username
su - $username <<'EOF'
	curl -s https://gitlab.com/vitusdsouza/hps_scripts/-/raw/master/wpversioncheck.php > wpversioncheck.php
	currentdir=$(pwd)
	php wpversioncheck.php $currentdir
	path=$(php wpversioncheck.php $currentdir | grep INFO | cut -d"/" -f2-)
	for i in $path
	do
	cd /$i
	curl -s https://gitlab.com/vitusdsouza/hps_scripts/-/raw/master/wpchecksum.php > wpchecksum.php
	echo " "
	echo "Matching WordPress Checksum for Path /$i"
	echo " "
	php wpchecksum.php
	rm -f wpchecksum.php
	cd
	done
	cd
	rm -f wpversioncheck.php
EOF
	echo "WordPress Checksum Matching Complete"
}



###############Mailman Archive Cleanup###############



function mailmancleanup() {
	get_cpanel_username
	mailmanpath=$(echo /usr/local/cpanel/3rdparty/mailman/archives/private)
	echo "Below mailman archives have been found for $truedomain: "
	mailmandisk
	echo " "
	read -p "Do you want to proceed with cleanup of Archives, Attachments & Mbox?" mboxchoice
	case $mboxchoice in
	[yY] )
		echo "Starting cleanup..."
		mailmandel
		/scripts/update_mailman_cache
		echo "Press ENTER To Continue..."
		read
		return
		;;
	[nN] )
		echo "Okay, no problem"
		return
		;;
	*)
		echo "Invalid Response. Aborting..."
		return
		;;
	esac
}

function mailmandisk () {
	list=$(ls $mailmanpath | grep $truedomain)
	for i in $list
	do
		du -hs $mailmanpath/$i
	done
}

function mailmandel() {
	archives=$(ls $mailmanpath | grep $truedomain | grep -v mbox)
	for k in $archives
	do
		cd $mailmanpath/$k
		rm -rf 2010* 2011* 2012* 2013* 2014* 2015* 2016* 2017* 2018* 2019* 2020* attachments/*
	done
	mboxdir=$(ls $mailmanpath | grep $truedomain | grep mbox)
	for j in $mboxdir
	do
		cd $mailmanpath/$j
		rm -f *.mbox
	done
	echo "Mailman Archives, Attachments & Mbox for the domain $truedomain have been cleaned"
	echo " "
	echo "The current mailman disk usage for $treudomain is: "
	mailmandisk
}



###############AUP###############



function aup () {
	get_cpanel_username
	cd $path 
	echo "Detailed Inode Usage for: $(pwd)" ; 
	for d in `find -maxdepth 1 -type d | cut -d\/ -f2 | grep -xv . |sort` 
	do c=$(find $d |wc -l)  
		printf "$c\t\t- $d\n" 
	done
	printf "Total: \t\t$(find $(pwd) | wc -l)\n"
	echo " "
	cd
	echo "Top 50 Entries will be Displayed for Disk Usage"
	echo "Total Disk Usage for: $(pwd)"
	du -h $path --max-depth=1 | sort -rh | head -n 50
	echo " "
	echo "Detailed Mail Disk Usage for: $(pwd)"
	du -h $path/mail/ --max-depth=2 | sort -rh | head -n 50
	echo " "
	echo "Largest Files under the Path $path: "
	find $path -printf '%s %p\n'| sort -nr | head -n 50
	echo "Press ENTER To Continue..."
	read
	return
}



###############StarWars###############



function starwars() {
	while :
	do
		clear
		echo " "
		echo " "
		curl -s https://gitlab.com/vitusdsouza/hps_scripts/-/raw/master/starwars
		echo " "
		echo " "
	    echo "[1] ClamAV Scan"
	    echo "[2] Parsed cPanel Acess Logs for Dummies"
	    echo "[3] Filtered Domain Access Logs for Dummies"
	    echo "[4] Find Recently Changed Files"
	    echo "[5] Find Recently Modified Files"
	    echo "[6] Quarantine Infected Files"
		echo "[7] WordPress Version & Core Integrity Check"
		echo "[8] Find & Clear Mailman Archives, Attachments, Mbox (untested)"
		echo "[9] AUP Violation Check & Details"
	    echo "[10] Exit"
		echo "Work in Progress:"
		echo "- Repair WordPress Installation"
		echo "- WPScan"
	    echo "====================================="
	    read -p "Enter your menu choice [1-10]: " menu
		case "$menu" in
			1) scan;;
			2) cplog;;
			3) domlog;;
			4) ctimelog;;
			5) mtimelog;;
			6) quarantine;;
			7) wpchecksum;;
			8) mailmancleanup;;
			9) aup;;
			10) exit 0;;
	        *) echo "Opps!!! Please Select Correct Choice";
			echo "Press ENTER To Continue..." ; read ;;
			esac
		done
}

###############
starwars
###############