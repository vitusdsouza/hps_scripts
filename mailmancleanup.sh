#!/bin/bash
#Author: Vitus D'Souza

###############Common###############

function get_cpanel_username {
	read -p "Enter CPanel Username or Domain Name: " namespace
	username=$(grep $namespace /etc/trueuserdomains | cut -d " " -f2)
	truedomain=$(grep $namespace /etc/trueuserdomains | cut -d":" -f1)
	path=$(grep $username /etc/userdatadomains | cut -d"=" -f9 | cut -d"/" -f1,2,3)
	if [[ -s "$truedomain" ]]; then
		echo -e "Enter a valid CPanel Username"
		return
	fi
}

###############Mailman Archive Cleanup###############

function mailmancleanup() {
	get_cpanel_username
	mailmanpath=$(echo /usr/local/cpanel/3rdparty/mailman/archives/private)
	echo "Below mailman archives have been found for $truedomain: "
	mailmandisk
	echo " "
	read -p "Do you want to proceed with cleanup of Archives, Attachments & Mbox?" mboxchoice
	case $mboxchoice in
	[yY] )
		echo "Starting cleanup..."
		mailmandel
		/scripts/update_mailman_cache
		echo "Press ENTER To Continue..."
		read
		;;
	[nN] )
		echo "Okay, no problem"
		exit 1
		;;
	*)
		echo "Invalid Response. Aborting..."
		exit 1
		;;
	esac
}

function mailmandisk () {
	list=$(ls $mailmanpath | grep $truedomain)
	for i in $list
	do
		du -hs $mailmanpath/$i
	done
}

function mailmandel() {
	archives=$(ls $mailmanpath | grep $truedomain | grep -v mbox)
	for k in $archives
	do
		cd $mailmanpath/$k
		rm -rf 2010* 2011* 2012* 2013* 2014* 2015* 2016* 2017* 2018* 2019* 2020* attachments/*
	done
	mboxdir=$(ls $mailmanpath | grep $truedomain | grep mbox)
	for j in $mboxdir
	do
		cd $mailmanpath/$j
		rm -f *.mbox
	done
	echo "Mailman Archives, Attachments & Mbox for the domain $truedomain have been cleaned"
	echo " "
	echo "The current mailman disk usage for $treudomain is: "
	mailmandisk
}


###############
mailmancleanup
###############