#!/bin/bash
#Author: Vitus D'Souza

############### DomLog Parser ###############
function domlog () {
	get_domlog_files
	for n in $domlog_files
	do 
	data=$(cat /usr/local/apache/domlogs/$n | grep POST | grep " 200 " | grep -v "wp-cron.php?doing_wp_cron" | egrep -v "115.110.127.198|122.15.255.69|115.110.71.146|182.73.214.22|111.93.159.50|220.227.162.29|115.110.126.75|182.74.165.114")
	if [[ -z $data ]]; then
		:
	else
		echo " "
		echo "Parsing the log file: " $n
		get_initial_timestamp
		echo "Logs parsed since:" $start_timestamp
		echo " "
		get_post_hit
		get_hit_timestamp
	fi
	done
}

function get_domlog_files () {
	domlog_files=$(ls /usr/local/apache/domlogs/ | grep $truedomain | grep -v ftp | grep -v bytes)
	if [[ $? -ne 0 ]]; then
		echo -e "Domain access log file missing on server. Kindly verify the CPanel Username."
		exit 1
	fi
}

function get_initial_timestamp () {
	start_timestamp=$(head -n 1 /usr/local/apache/domlogs/$n | awk '{print $4,$5}')
}

function get_post_hit () {
	post_hit=$(cat /usr/local/apache/domlogs/$n | grep POST | grep " 200 " | cut -d" " -f7 | sort | uniq | cut -d"/" -f2-100 | grep -v "wp-cron.php?doing_wp_cron")
}

function get_country () {
	stat /usr/bin/whois>/dev/null 2>&1
	if [[ $? -ne 0 ]]; then
		country=" "
	else
		country=$(whois $ip | grep country | tail -n 1 | awk '{print $2}')
	fi
}

function get_hit_timestamp () {
	for t in $post_hit
	do
		iptime=$(grep $t /usr/local/apache/domlogs/$n | grep POST | grep " 200 " | tail -n 1 | awk '{print $1,$4,$5}')
		ip=$(echo $iptime | cut -d" " -f1)
		timestamp=$(echo $iptime | cut -d" " -f2)
		hit_count=$(grep $t /usr/local/apache/domlogs/$n | grep POST | grep " 200 " | wc -l)
		get_country
		echo "$ip" "$country" "$timestamp]" "[Hits:$hit_count]" $t
	done
}
############### DomLog Parser ###############

############### Run Script ###############
[ $# -eq 0 ] && { echo "Usage: $0 <cpanel_username/domain_name>"; exit 1; }
username=$(grep $1 /etc/trueuserdomains | cut -d " " -f2)
if [ -z "$username" ]; then
	echo "Usage: $0 <cpanel_username/domain_name>"; 
	exit 1
fi
truedomain=$(grep $1 /etc/trueuserdomains | cut -d":" -f1)
path=$(grep $username /etc/userdatadomains | cut -d"=" -f9 | cut -d"/" -f1,2,3)
domlog
############### End Script ###############