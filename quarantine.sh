#!/bin/bash
#Author: Vitus D'Souza

############### Get Input ###############

function get_cpanel_username {
	read -p "Enter CPanel Username or Domain Name: " namespace
	username=$(grep $namespace /etc/trueuserdomains | cut -d " " -f2)
	truedomain=$(grep $namespace /etc/trueuserdomains | cut -d":" -f1)
	path=$(grep $username /etc/userdatadomains | cut -d"=" -f9 | cut -d"/" -f1,2,3)
	if [[ -s "$truedomain" ]]; then
		echo -e "Enter a valid CPanel Username"
		return
	fi
}

###############Quarantine###############



function quarantine () {
	read -p "Did you confirm the Reseller Support PIN? [Y/n]: " pin
	case $pin in
	[yY] )
		get_cpanel_username
		move_malware
		;;
	[nN] )
		echo "Nope, Not Happening. You need to confirm the 4-digit support PIN."
		return
		;;
	*)
		echo "Invalid Response. Aborting..."
		return
		;;
	esac
}

function move_malware () {
	malware=$(find $path -maxdepth 1 -type f -name malware.txt)
	stat $path/malware.txt>/dev/null 2>&1
	if [[ $? -ne 0 ]]; then
		echo "File $path/malware.txt Not Found. Kindly run a ClamAV Scan First."
		return
	fi
	mkdir $path/quarantine
	chown $user:$user $path/quarantine
	if [[ $? -ne 0 ]]; then
		echo "Unable to create the quarantine directory"
		return
	else
		echo "The quarantine directory is: $path/quarantine"
	fi
	echo "Starting the Quarantine Process..."
	cat $path/malware.txt | awk '{print $1}' | sed 's/.$//' | grep $path > $path/quarantine/list
	num=0
	for q in `cat $path/quarantine/list`
	do
		quarantine_dir=$(dirname `echo $q` | cut -d"/" -f4-)
		mkdir -p $path/quarantine/$quarantine_dir
		mv $q $path/quarantine/$quarantine_dir/
		((num=num+1))
	done
	find $path/quarantine/ -type d -exec chmod 750 {} \;
	find $path/quarantine/ -type f -exec chmod 640 {} \;
	echo "$num Infected Files have been moved to $path/quarantine"
	chown -R $user:$user $path/quarantine
}

###############
quarantine
###############